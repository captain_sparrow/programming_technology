/*
Создать класс «Банковская карта», включающий данные-элементы:
 - номер,
 - имя владельца,
 - количество денег на счете,
 - пин-код.
Функции-элементы:
    - создание и инициализация (конструктор),
    - приход (сумма – в аргументе),
    - расход (сумма – в аргументе, обязательно анализ правильности ввода пин-кода и остатка, минусовой баланс запрещен),
    - выдача сообщения об остатке средств на счете (обязательно анализ правильности ввода пин-кода),
    - деструктор.
 * */

#include <iostream>
//#include <string>

using namespace std;

class BankCard {
private:
    string number;
    string name;
    string surname;
    float balance = 0;
    int pincode = 1234;

public:
    BankCard (string _number, string _name, string _surname) :
            number(_number), name(_name), surname(_surname) {};

    void receipt(float sum)
    {
        balance += sum;
    }

    void withdraw(float sum, int pin)
    {
        if (pin != pincode)
            throw logic_error("Verification pincode failed. Recipient not authorized.\n");

        if (sum > balance)
            throw logic_error("Not enough money.\n");

        balance -= sum;
    }

    float getBalance(int pin)
    {
        if (pin != pincode)
            throw logic_error("Verification pincode failed. Recipient not authorized.");

        return balance;
    }

    ~BankCard(){};
};

int main() {
    BankCard *bankCard = new BankCard("214211212", "Alexander", "Vorobey");
    bankCard->receipt(10000);
    bankCard->withdraw(100, 1234);
    float balance = bankCard->getBalance(1234);
    cout << balance;
    delete bankCard;
    return 0;
}