## Link to download dependencies:
[Download staruml](http://staruml.io/download) 

[Download libgcrypt11](https://packages.debian.org/wheezy/amd64/libgcrypt11/download)

```shell
sudo dpkg -i libgcrypt11_1.5.0-5+deb7u6_amd64.deb
sudo apt-get install ./StarUML-v2.8.1-64-bit.deb
```
